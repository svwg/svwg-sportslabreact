import React from "react"







function Team(props){

 
    const { logo, name } = props
  
    const shotPercentage= Math.round((props.stats.score / props.stats.shotsTaken) * 100)
    //this.state.shotsTaken
      
    
    // let shotsTaken = this.state.shotsTaken;
    // let score = this.state.score;
    return (
      <div>
        <h2>{name}</h2>
        <div className="MyTeams">
          <img src={logo} alt={name} width="400px" height="400px" />
        </div>
        <div>ShotsTaken: {props.stats.shotsTaken}</div>
        <div>Score: {props.stats.score}</div>
        {props.stats.shotsTaken > 0 && <div>Shot Percentage: {shotPercentage}%</div>}
        <button onClick={props.shootButton}>Shoot</button>
      </div>
    );
  }

  export default Team
  