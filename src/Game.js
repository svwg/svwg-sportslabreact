import React from "react"
import Team from './Team'
import ScoreBoard from './ScoreBoard'
import BackBoard from './Back+Board.wav'
import Swish2 from './Swish+2.wav'


class Game extends React.Component {
    constructor(props){
      super(props)
  
      this.state = {
        resetCount:0,
        homeTeamStats:{
          shotsTaken: 0,
          score: 0
        },
        visitingTeamStats:{
          shotsTaken:0,
          score: 0
        }
        
      }
      this.shotsTakenSound = new Audio(BackBoard)
     this.scoreSound = new Audio(Swish2)
      
    }
  
  
    shootButton = (team) => {  
      const teamStatsKey = `${team}TeamStats`
      //console.log(teamStatsKey)
      let score = this.state[teamStatsKey].score
      this.shotsTakenSound.play()
      if (Math.random() > 0.5) {
        score += 1
       // this.scoreSound.play();
        setTimeout(() =>{
          this.scoreSound.play()
        }, 1000)
      }
     this.setState((state, props) => ({ 
       [teamStatsKey]:{
          shotsTaken: state[teamStatsKey].shotsTaken + 1,
          score
        }
        }))
      
    }
    //  scoreCounter = () =>{
    //    {this.state.score + 1}
    //  }
    resetGame = () => {
      this.setState((state, props)  => ({
        resetCount: state.resetCount + 1,
        homeTeamStats: {
          shotsTaken: 0,
          score: 0
        },
        visitingTeamStats: {
          shotsTaken: 0,
          score: 0
        }
       }))
    }
  
  
  
  
      render(){
        return (
    <div className="Game">
      <ScoreBoard visitingTeamStats={this.state.visitingTeamStats} homeTeamStats={this.state.homeTeamStats}/>
        <h1>Welcome to {this.props.venue}</h1>
        <div className="Team">
          <Team name={this.props.homeTeam.name} logo={this.props.homeTeam.logo} stats={this.state.homeTeamStats} shootButton={()=> this.shootButton('home')} />
        </div>
        <div className="header">
        <h3>Against</h3>
        <div>
          Resets:{this.state.resetCount}
          <button onClick={this.resetGame}>Reset Game</button>
        </div>
        </div>
        <div className="Team">
          <Team name={this.props.visitingTeam.name} logo={this.props.visitingTeam.logo} stats={this.state.visitingTeamStats} shootButton={()=> this.shootButton('visiting')}/>
        </div>
     </div>
      
    )
    }
  }

  export default Game